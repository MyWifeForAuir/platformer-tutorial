tool
extends Area2D

export var next_scene: PackedScene

func _get_configuration_warning() -> String:
	if not next_scene:
		return "The next scene property can't be empty"
	return  ""

func _ready():
	pass # Replace with function body.
	
func _on_player_entered(body):
	teleport()
	
func teleport():
	var animation_player = get_node("AnimationPlayer") # $AnimationPlayer to samo co get_node
	animation_player.play("fade_out")
	yield(animation_player, "animation_finished") # pauzuje caly kod az dostanie sygnal
	get_tree().change_scene_to(next_scene)
	return

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


