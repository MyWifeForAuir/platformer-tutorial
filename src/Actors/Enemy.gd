extends "res://src/Actors/Actor.gd"

func _ready() -> void:
	set_physics_process( false )
	_velocity.x = -speed.x
	
func _on_StompDetector_body_entered(body: PhysicsBody2D):
	var detector = get_node("StompDetector")
	if body.global_position.y > detector.global_position.y:
		return
	get_node("CollisionShape2D").disabled = true
	queue_free() # usuwanie enemy

func _physics_process(delta: float) -> void:
	update_direction()
	_velocity = calc_move_velocity(_velocity )
	# jezeli przypisze tez velocity.x to resetuje odbijanie sie od scian, dlaczego?
	_velocity.y = move_and_slide(_velocity, FLOOR_NORMAL).y 
	return

func update_direction() -> void:
	var next_tile_pos = position
	next_tile_pos.x += _velocity.x * 0.3
	var next_tile_transform = Transform2D(rotation,next_tile_pos)
	var is_hole_in_front = not test_move(next_tile_transform, Vector2.DOWN)
	
	if is_on_wall() or is_on_floor() and is_hole_in_front:
		_velocity.x *= -1.0

func calc_move_velocity(
		linear_velocity: Vector2
		#direction: Vector2,
		#speed: Vector2,
	) -> Vector2:
	var out = linear_velocity
	out.y += gravity * get_physics_process_delta_time()
	#out.x = direction.x * speed.x
	return out
