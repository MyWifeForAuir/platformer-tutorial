extends Actor

export var stomp_impulse = 1000.0
export var DIE_POS_Y = 650 

func _on_EnemyDetector_area_entered(area: Area2D) -> void:
	_velocity = calc_stomp_velocity(_velocity, stomp_impulse)
	
func _on_EnemyDetector_body_entered(body: PhysicsBody2D) -> void:
	die()

func _physics_process(delta: float) -> void:
	var direction = get_direction()
	var is_jump_interrupted: = Input.is_action_just_released("jump") and _velocity.y < 0.0
	_velocity = calc_move_velocity(_velocity, direction, speed, is_jump_interrupted)
	_velocity = move_and_slide(_velocity, FLOOR_NORMAL)
	
	if position.y > DIE_POS_Y:
		die()
	return
	
func get_direction() -> Vector2:
	var direction: = Vector2.ZERO
	if Input.is_action_pressed("move_left"):
		direction.x = -1.0
	elif Input.is_action_pressed("move_right"):
		direction.x = 1.0
	else:
		direction.x = 0.0
		
	if Input.is_action_pressed("jump") and is_on_floor():
		direction.y = -1.0
	else:
		direction.y = 1.0
	return direction

func calc_move_velocity(
		linear_velocity: Vector2,
		direction: Vector2,
		speed: Vector2,
		is_jump_interrupted: bool
	) -> Vector2:
	var out = linear_velocity
	out.y += gravity * get_physics_process_delta_time()
	out.x = direction.x * speed.x
	if direction.y == -1.0: # nadanie predkosci gdy klikne skok
		out.y = speed.y * direction.y
	if is_jump_interrupted:
		out.y = 0.0
	return out

func calc_stomp_velocity( linear_velocity: Vector2, impulse: float) -> Vector2:
	var out = linear_velocity
	out.y = -impulse
	return out
	
func die():
	queue_free()
