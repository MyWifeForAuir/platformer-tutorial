tool #dzieki temu odpalam kod w edytorze i dzisla get_configuration_warning
extends Button

export(String, FILE) var next_scene_path = ""

func _on_button_up():
	get_tree().change_scene(next_scene_path)
	
func _get_configuration_warning() -> String:
	if not next_scene_path:
		return "The scene path property can't be empty"
	return  ""
